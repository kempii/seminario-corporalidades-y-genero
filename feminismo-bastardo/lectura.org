* Feminismo bastardo
De: María Galindo

Memoria: https://pad.riseup.net/p/seminario-corporalidades-y-genero-keep

** Comentarios generales

- Me parece extraño que el libro sea difícil de conseguir, al parecer no existe el libro electrónico.
- Me parece interesante que el libro tenga licencia Creative Commons.
- La edición de Editorial Mantis y Canal Press tuvo la intención de que el libro llegara a México y a Estados Unidos.

** Prólogo
 - Me parece interesante que Paul Preciado sea quien abre el libro. ¿Por qué una persona de este lado del continente?

** Capítulo 1: El feminismo no es un proyecto de derechos para las mujeres, es un proyecto de transformación social

*** Bastarda

**** Bautizo

- Me parece sumamente simbólico que empieze con la analogía del bautizo (pg. 31) haciendo memoria de la colonización como una violación no sólo de la tierra y el territorio sino de los cuerpos.

#+BEGIN_QUOTE
Se clasifica el modo de vestir, el comportamiento, el color de piel, el modo de reír, el modo de hablar, el modo de comer y cada detalle del cuerpo y la vida. La lógica de esta clasificación es evidentemente racista y misógina, pero no revela únicamente parámetros racistas de color de piel; revela también los profundos resentimientos en torno de la circulación del deseo erótico; delata y burla las tiranías estéticas; esconde o revela las servidumbres sexuales más antiguas. Quiere decir que estas clasificaciones, como popularmente se dice, te llegan al alma. (pg. 32)
#+END_QUOTE

- María Galindo habla de la clasificación como una práctica social. Agregando, me parece que hace referencia a que esta práctica social es aprendida y se hace en automático mediante la estructura racista y colonial.
- También, María Galindo habla de que las clasificaciones no son "cubículos separados nítidamente, sino tensiones sociales que atraviesan todos los aspectos de la vida y todos los cuerpos sin excepción".
- También habla de que las clasificaciones y vigilancias "implican el impedimiento del tránsito de un lugar a otro, de ida y vuelta".

#+BEGIN_QUOTE
No estamos hablando única y simplemente de una supremacía blanca que construye clasificaciones del <<otro>> bajo un código racista de jerarquías de belleza y valor; estamos hablando de una construcción más compleja donde se hace difícil sino imposible separar racismo de circulación y vigilancia del deseo erótico, donde se hace improsible separar racismo de colonialismo y donde se hace imposible comprender esas estructuras sin dilucidar la clasificación de los cuerpos y las sexualidades. (pg. 33-34)
#+END_QUOTE

- Me parece importante que María Galindo haga la distinción para señalar la complejidad y la gravedad de la estructura.

**** El mestizaje como proyecto estatal colonial

#+BEGIN_QUOTE
Son estas prácticas y profundos complejos sociales los que dejan al descubierto y en evidencia lo que Raquel Gutiérrez llama la <<herida colonial>>; una herida abierta y sangrante que duele y que el proyecto de mestisaje no ha cerrado sino que ha tapado sin curar o profundizado sin resolver. (pg. 34)
#+END_QUOTE

- De alguna manera, la herida provoca que nosotrxs la sigamos reproduciendo. ¿Cómo le hacemos para curar esa herida?

#+BEGIN_QUOTE
Nos hemos empantanado en la pregunta sobre qué tipo de Estado queremos y hemos evadido la pregunta de qué tipo de relaciones sociales tenemos y qué tipo de relaciones sociales queremos construir, más allá y más acá del Estado. (pg. 34)
#+END_QUOTE

- El Estado se ha presentado y concebido como indispensable, y nos ha llevado a pensarlo entrañable a nosotrxs. Sin embargo, María Galindo nos abre la posibilidad a salirnos de él y pensar y entrañar lo que realmente nos construye como comunidad, las relaciones sociales.
- También, María Galindo habla del "mestizaje como blanqueamiento" para justificar al Estado. Me parece imprescindible esta reflexión porque un Estado no es legítimo, desde la lectura colonial, si no es blanco o si de menos no está blanqueado.

#+BEGIN_QUOTE
El Estado ha hecho centenarios esfuerzos por construir el discurso del Estado/Nación como pertenencia a partir del fetichismo nacionalista; a partir del fetichismo militarista y a partir, sin duda, del relato de la historia y la cultura como aquello que debe representar el sentido de pertenencia. (pg. 35)
#+END_QUOTE

- María Galindo apunta que la ficción del Estado/Nación funciona debido a que nos hace creer que nos pertenece. Sin embargo, nos has creer que nos pertenece después de habernos arrebatado todo con violencia.
- A partir de esto, plantea "salir de los marcos de definición del Estado/Nación" tanto para cuestionar la estructura como para salirse de la lógica estatalista desde la reflexión.

**** Ni interculturalidad, ni diversidad, ni plurinacionalidad

#+BEGIN_QUOTE
No es precisamente un modelo de Estado lo que está en discusión, sino las relaciones sociales vistas más allá de las categorías de clase, raza y género. (pg. 36)
#+END_QUOTE

- La ficción del Estado nos hace creer que no nos relacionamos a partir de las categorías de clase, raza y género. Las oculta para hacernos creer que todxs somos iguales frente al Estado.
- Es por ello que es imprescindible salirnos del Estado para pensarnos y relacionarnos desde las categorías de clase, raza y género.

#+BEGIN_QUOTE
Critico a su vez la categoría de diversidad como lugar de afirmación de la diferencia porque el /diverso/ es siempre el /otro/. La lógica y la categoría de la diversidad no rompen con la hegemonía colonial de lo universal; mantienen el juego de poder de la universalidad versus la diversidad... (pg. 37)
#+END_QUOTE

- Me parece que esta crítica acierta particularmente cuando el Capital se apropia del discurso de la diversidad debido a que toma esa diversidad del otro y lo satisface. La diversidad no significa inclusión.
- Asimismo, María Galindo critíca las luchas llamadas <<indígenas>>, el modelo de interculturalidad y la plurinacionalidad, desde sus refritos apropiados capitalistas/estatales.

**** ¿Si nos preguntamos no por el objeto de deseo, sino por el objeto de odio?

#+BEGIN_QUOTE
El problema no es la búsqueda de un modelo de relacionamiento con el otro, sino el odio profundo contra la india que llevamos dentro: dentro y debajo de la piel, dentro y en las entrañas de nuestra subjetividad, dentro y en el inconsciente de nuestros sueños, dentro y en la olvidada lengua materna, dentro y en el sótano de nuestros recuerdos. (pg. 38)
#+END_QUOTE

- La estructura colonial, o más bien, la <<herida colonial>> nos moldea e influencia nuestras interacciones con nosotrxs mismxs y con lxs demás.

#+BEGIN_QUOTE
...¿cómo y dónde disolver, resolver, desmenuzar, triturar y picar ese odio hasta convertirlo en partículas mínimas destinadas a desaparecer? (pg. 39)
#+END_QUOTE

**** La vergüenza y el castigo como origen. Contrabando de identidades

#+BEGIN_QUOTE
Por razones de espacio, consciente de que estoy simplificando el escenario, voy a plantear dos corrientes de tensión fundamentales que se colocan en lados opuestos, que son complementarias por oposición y que hacen de los cuerpos de las mujeres el campo de batalla principal: blanqueamiento/barbificación versus indianización/originarismo. (pg. 39)
#+END_QUOTE

- ¿Dónde podemos ver operando este binarismo blanqueamiento-indianización?

**** El eterno retorno a un pasado que no hay

#+BEGIN_QUOTE
Experimento y planteo el lugar bastardo como un espacio de huida de ese binarismo, como un espacio de legitimación de la desobediencia y la crítica cultural en todos los sentidos. (pg.40)
#+END_QUOTE

- Me parece que la propuesta de la bastarda desafía el binarismo blanqueamiento-indianización debido a que escapa la urgencia de la purificación hacia cualquier lado del binarismo.
- También, me parece que la propuesta de María Galindo apunta particularmente a criticar (por supuesto al blanqueamiento pero también) la indianización como un proyecto de pureza que replica las lógicas coloniales y patriarcales.
- Sin embargo, me parece que las reflexiones desde los feminismos comunitarios y feminismos indígenas representan una interlocución frente a las reflexiones/críticas de María Galindo.

#+BEGIN_QUOTE
Huyo de la disputa por el origen y la autenticidad y denuncio esa disputa como absurda, desgastante y cruel. El origen no existe como tal y tampoco la autenticidad, somos un viaje de metamorfosis continua que es intrínseca a la vida misma. (pg. 41)
#+END_QUOTE

- ¿Cuál es nuestro origen? ¿De dónde venimos? ¿Existe un lugar único o somos una combinación de lugares y factores?

#+BEGIN_QUOTE
La memoria que la bastarda activa es la memoria del conflicto, la memoria de la escena de violación como origen. (pg. 42)
#+END_QUOTE

- Hacer memoria a través del bastardismo es remembrar la violación como origen, es habitar la <<herida colonial>> y construir nuevos mundos y subjetividades que escapan de ella.

**** Piel negra, máscaras blancas

#+BEGIN_QUOTE
La racialización es la pigmentación política de la piel, no es el color de la piel. No hay que olvidarlo. No nos toca jugar ese juego, sino sabotearlo. (pg. 43)
#+END_QUOTE

- La racialización, como la pigmentación política de la piel, depende del contexto de cada lugar, y construye relaciones de poder a partir, me parece, de la blanquitud/blanqueamiento.

#+BEGIN_QUOTE
Las luchas descolonizadoras y antirracistas necesitan abrir un nuevo círculo conceptual que trascienda los límites identitarios, que trascienda los relatos victimistas, que transcienda el mito originario. El lugar desde donde hacerlo es el que Gloria Anzaldúa nombró como /frontera/ y que me atrevo a re/nombrar como bastardismo. (pg. 43)
#+END_QUOTE

- ¿A qué se refiere María Galindo con trascender los límites identitarios? ¿Qué crítica, desde dónde lo critica y por qué lo critica?

*** Bibliografía feminista imprescindible o atrévete a leer feminismo

- La bibliografía propuesta invita justo a salirse de la academia y a habitar, no sólo la crítica y la reflexión, sino el bastardismo que propone María Galindo.
- ¿Por qué esta bibliografía es relevante e importante? ¿Y qué deja resonando en nosotrxs?

*** Feminismo sísmico, sismo feminista, feminismo

#+BEGIN_QUOTE
...sí es muy importante en la escritura: ¿Desde dónde estás hablando? Desde la calle, desde dentro de un movimiento, desde el micrófono de una radio, desde una cooperativa de autogestión, desde una incertidumbre existencial, desde el afuera de toda institución. (pg.49)
#+END_QUOTE

- Me parece similar a propuestas interseccionales donde se vuelve sumamente relevante posicionarse y contextualizar la reflexión y la acción.
- ¿Desde dónde estamos hablando nosotrxs?

**** El feminismo como alianza ética no ideológica

#+BEGIN_QUOTE
En el caso del feminismo y partiendo de que no hay un solo feminismo sino tantos feminismos como diferentes visiones, diferentes prácticas políticas, diferentes composiciones sociales, el desacuerdo es una constante y esa es su mayor potencia política. No estamos de acuerdo, no pensamos igual y, sin embargo, confluimos en eso que se llama feminismo y cuya definición y límite no es propiedad de nadie. Esa es la potencia mayor y, aparentemente, también su mayor debilidad. (pg. 50)
#+END_QUOTE

- Los feminismos no son binarios, lo que permite que estén abiertos a cualquier visión y contexto.
- Sin embargo, dependiendo el contexto, también dentro de los feminismos se ejercen relaciones de poder.

#+BEGIN_QUOTE
La idea de que hay una sola verdad -y de que toda verdad se expresa en antagonismos basados en la lógica formal de que lo positivo para ser tal es contrario a lo negativo, lo negro a lo blanco, lo bueno a lo malo- nos instala en una lógica binaria donde la complejidad no es posible, es incorrecta e indeseable [...] Esa idea única, monolítica, esencialista y que es <<propiedad de Dios>>, es fundante del patriarcado mismo, de la forma de gobierno, de la concepción misma de la lucha social como imposición de una única verdad posible que debe ser hegemónica. (pg. 50)
#+END_QUOTE

- Entiendo que la propuesta es empezar a salirse de los binarios para empezar a salirse de las lógicas patriarcales y estatales.

#+BEGIN_QUOTE
No pongamos en discusión cómo entendemos el feminismo, sino cuáles son las prácticas políticas que lo sustentan, eso traslada la discusión exactamente a las formas cómo construimos feminismos. (pg. 51)
#+END_QUOTE

- Me parece que tomar como base las formas y no el fondo es sumamente interesante. Generalmente concebimos las luchas sociales con base en un fondo, y es lo que históricamente ha unido a los movimientos sociales y políticos, las ideas.
- Creo que resultaría complejo lograrlo, particularmente porque estamos programadxs para hacer frente a posturas políticas, y dentro de los feminismos hay posturas políticas bastante diferentes.

**** La confluencia feminista

#+BEGIN_QUOTE
El desacuerdo enriquece, la deformidad/no uniformidad de los feminismos enriquece, pero necesitamos un punto de confluencia [...] Un hilo que nos permita leernos y reconocernos unas a otras y otras sin perder las diferencias... (pg.52)
#+END_QUOTE

- "¿Cómo construir una ocnfluencia feminista entonces?"

#+BEGIN_QUOTE
Permítanme decirles que la DESPATRIARCALIZACIÓN es esa palabra, es ese lugar, es esa llave, es ese concepto que puede englobar, cohesionar, abrir un nuevo sentido de época [...] Lo nuestro no es un proyecto de derechos, es un proyecto de transformación de estruturas; y la despatriarcalización como horizonte de época refleja precisamente eso. Es una gran puerta donde caben caóticamente todas nuestras luchas. (pg. 53)
#+END_QUOTE

- Me resuena, pero también me hace pernsar en los movimientos anti-patriarcales que buscan la transformación de estructuras, pero que precisamente no se nombran feministas por lo que representan todos aquellos feminismos conservadores y anti-derechos.

**** Feminismo intuitivo versus academicismo

#+BEGIN_QUOTE
Lo que planteo es que ese feminismo de la calle tiene nombre y se llama /feminismo intuitivo/. No responde ni a una instrucción ideológica ni a una lectura académica, sino que responde a una decisión existencial y a una lectura directa y vivencial de su cuerpo, de la calle, del barrio, de la cárcel, de los juzgados, del desempleo.

No es un feminismo carente de discurso, sino uno cuyas protagonistas son voces silenciadas, sin foro ni micrófono. (pg. 54)
#+END_QUOTE

- Reconocer y nombrar el feminismo intuitivo permite darle voz a las morras que ponen el cuerpo en la calle.

#+BEGIN_QUOTE
Las alianzas étnicas (no ideológicas) [...] pueden ser legítimas, pueden ser espontáneas, pueden ser coyunturales. La pregunta es si son subversivas, si nos permiten repensar los feminismos y construir nuevos lenguajes y nuevos marcos conceptuales que no sean el marco de derechos, ni de leyes, ni de cuotas, ni de inclusión, sino de revolución. (pg. 55)
#+END_QUOTE

- Entonces, ¿cómo lograr que estas alianzas sean subversivas?

**** Hacia dónde va y hacia dónde debería ir el feminismo

#+BEGIN_QUOTE
El feminismo nunca debería convertirse en un singular, sino mantener una estructura plural que le da su marco infinito de acción. (pg. 55)
#+END_QUOTE

- Esa pluralidad, en lo personal la entiendo como interseccional.
